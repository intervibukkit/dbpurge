import java.io.File;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class Main {
	
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("-------- Запуск: --------");
			System.out.println("java -jar DBPurge.jar [база данных] [кол-во дней]");
			System.out.println("Пример: java -jar DBPurge.jar /root/auths.db 30");
			System.out.println("-------- Created by InterVi --------");
		} else {
			File check = new File(args[0].trim());
			if (!check.isFile()) System.out.println("БД не найдена"); else {
				int d = 0;
				boolean ok = true;
				try {
					d = Integer.parseInt(args[1].trim());
				} catch(Exception e) {
					ok = false;
					System.out.println("-------- Ошибка: --------");
					System.out.println("java -jar DBPurge.jar [база данных] [кол-во дней]");
					System.out.println("Неверное кол-во дней");
					System.out.println("-------- Created by InterVi --------");
					e.printStackTrace();
				}
				if (ok) onPurge(args[0].trim(), d);
			}
		}
	}
	
	private static void onPurge(String db, int days) {
		System.out.println("-------- DBPurge, created by InterVi --------");
		System.out.println("Началась чистка, чтение БД...");
		long stamp = System.currentTimeMillis();
		ArrayList<String> list = new ArrayList<String>();
		try {
			BufferedReader text = new BufferedReader(new FileReader(db));
			String line = null;
			int l = 0;
			do {
				line = text.readLine();
				if (line != null) {
					l++;
					long stamp2 = 0;
					try {
						stamp2 = Long.parseLong(line.split(":")[3]);
					} catch(Exception e) {System.out.println("Багнутая строка на ник: " + line.split(":")[0]);}
					long ch = (stamp / 1000 / 60 / 60 / 24) - (stamp2 / 1000 / 60 / 60 / 24);
					if (ch >= days) {
						if (stamp != 0) {
							System.out.println("Удаляем " + line.split(":")[0] + ", не заходил " + String.valueOf(ch) + " дней");
						}
					} else list.add(line);
				}
			} while (line != null && l < 2147483647);
			text.close();
		} catch(Exception e) {e.printStackTrace();}
		if (list.size() > 0) {
			System.out.println("Запись в файл...");
			try {
				File file = new File(db);
				File fnew = new File(db + ".old");
				System.out.println("Создание бэкапа: " + db + ".old");
				file.renameTo(fnew);
				BufferedWriter text = new BufferedWriter(new FileWriter(db));
				for (int i = 0; i < list.size(); i++) {
					if (i != 0) text.newLine();
					text.write(list.get(i));
				}
				text.close();
			} catch(Exception e) {e.printStackTrace();}
		} else System.out.println("БД пуста");
		long stamp3 = System.currentTimeMillis();
		double sec = (double) (stamp3 - stamp) / 1000;
		System.out.println("Чистка заняла: " + String.valueOf(sec) + " секнуд");
		System.out.println("Готово!");
	}

}